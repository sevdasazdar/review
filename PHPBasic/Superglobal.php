<?php
if (isset($_REQUEST['submit'])) {
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if (!empty($_REQUEST['fname']) && !empty($_REQUEST['lname']))
            echo "<p class='ok'>hello " . $_REQUEST['fname'] . " " . $_REQUEST['lname']."</p>";
    }else{
        echo "<p class='error'>yor request in not valid!</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .ok{
            color: green;
            background-color: aquamarine;
            text-align: center;
        }
        .error{
            color: ghostwhite;
            background-color: red;
            text-align: center;
        }
    </style>
</head>

<body>
    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
        <label for="fname">First name:</label><br>
        <input type="text" name="fname" placeholder="enter your name..."><br>
        <label for="lname">Last name:</label><br>
        <input type="text" name="lname" placeholder="enter your lastname..."><br><br>
        <input type="submit" name="submit" value="Submit">
    </form>

    <p>If you click the "Submit" button, the form-data will be sent to a page called "/action_page.php".</p>



</body>

</html>