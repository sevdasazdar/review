<?php

namespace App;
session_start();
$_SESSION['username'] = "ami";

class test{
    function myfunction()
    {
        echo __FILE__ . "<br>";
        echo __LINE__ . "<br>";
        echo __DIR__ . "<br>";
        echo __FUNCTION__ . "<br>";
        echo __CLASS__ . "<br>";
        echo __METHOD__ . "<br>";
        echo __NAMESPACE__ . "<br>";
    }
}
$t = new test();
$t->myfunction();
// echo __FILE__ . "<br>";
// echo __LINE__ . "<br>";
// echo __DIR__ . "<br>";
// echo __FUNCTION__ . "<br>";
// echo __CLASS__ . "<br>";
// echo __METHOD__ . "<br>";
// echo __NAMESPACE__ . "<br>";



if (isset($_REQUEST['submit']))
    session_destroy();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
        <input type="submit" name="submit" value="destroy">
    </form>

    <?php if (isset($_SESSION))
        var_dump($_SESSION);
    ?>
</body>

</html>